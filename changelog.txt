---------------------------------------------------------------------------------------------------
Version: 1.0.6
Date: 23. 06. 2019
  Balancing:
    - Hardened furnaces technology 1 & 2 time unit 1 ==> 30
    - Hardened furnace technology 3 time unit 1 ==> 15
    - Compressor technology time unit 1 ==> 15
  Fixes:
    - Disabled hardened electric mining drill by default.
---------------------------------------------------------------------------------------------------
Version: 1.0.5
Date: 09. 06. 2019
  Fixes:
    - Fixed hardened stone and steel furnaces recipe ingredients.
---------------------------------------------------------------------------------------------------
Version: 1.0.4
Date: 09. 06. 2019
  Fixes:
    - Fixed crafting categories issue with boblogistics.
---------------------------------------------------------------------------------------------------
Version: 1.0.3
Date: 09. 06. 2019
  Fixes:
    - Fixed error caused by incompatibility with boblogistics.
---------------------------------------------------------------------------------------------------
Version: 1.0.2
Date: 08. 06. 2019
  Fixes:
    - Fixed icons for coal piece, sawdust and compressed fuel.
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 08. 06. 2019
  Fixes:
    - Fixed plutonium item names for integration with Plutonium Energy.
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 01. 06. 2019
  Major Features:
    - Added nuclear long-handed inserter.
    - Added nuclear stack inserter.
    - Added nuclear stack filter inserter.
    - Added modules tier 5 and 6
    - Added integration with Plutonium Energy.
    - Added plutonium steel.
    - Added plutonium assembling machine.
    - Added plutonium inserters.
    - Added plutonium transport belt.
  Minor Features:
    - Updated locale files.
    - Remade DeadLock integration.
  Balancing:
    - Fixed a lot of typos that affect balancing.
    - Module technologies rebalancing.
    - Productivity module 4 pollution bonus 0.125 ==> 0.14
    - Productivity-effectivity module 2 pollution bonus 0.075 ==> 0.07
    - Productivity-effectivity module 4 pollution bonus 0.125 == 0.14
    - Speed-productivity module 4 consumption bonus 2 ==> 1.8
    - Speed-productivity module 4 module productivity bonus 0.12 ==> 0.16
    - Speed-productivity module 2 pollution bonus 0.075 ==> 0.07
    - Speed-productivity module 4 pollution bonus 0.15 ==> 0.14
    - Hardened furnaces are now upgrades of regular furnaces.
    - Macerator and compressor crafting speed 0.75 ==> 1
    - Macerator and compressor energy usage 300kW ==> 150kW
    - Nuclear assembling machine energy consumption 1000kW ==> 1125kW
    - Nuclear assembling machine recipe crafting time 0.5 ==> 10
    - Nuclear assembling machine recipe crafting category changed to "advanced crafting".
    - Removed coal compressing recipe.
    - Changed hardened electric furnace crafting time from 0.5 to 5
    - Raw nuclear metal now can be crafted only through centrifuge.
    - Changed nuclear inserter recipe requester_paste_multiplier from 5 to 30
    - Changed nuclear automation prerequisite from Nuclear power to Uranium processing.
    - Changed nuclear logistics prerequisite from Nuclear power to Uranium processing.
  Graphics:
    - Updated nuclear assembling machine textures.
    - Updated nuclear automation technology icon
---------------------------------------------------------------------------------------------------
Version: 0.15.4
Date: 25. 04. 2019
  Fixes:
    - Fixed nuclear inserter entity icon.
---------------------------------------------------------------------------------------------------
Version: 0.15.3
Date: 24. 04. 2019
  Fixes:
    - Fixed nuclear underground belt side loading sprite.
---------------------------------------------------------------------------------------------------
Version: 0.15.2
Date: 24. 04. 2019
  Fixes:
    - Fixed hardened steel furnace lr texture filename.
---------------------------------------------------------------------------------------------------
Version: 0.15.1
Date: 14. 04. 2019
  Locale:
    - Russian: Ядрённый сборщик ==> Ядрённый сборочный автомат.
    - Ukrainian: Ядерний складальник ==> Ядерний складальний автомат.
---------------------------------------------------------------------------------------------------
Version: 0.15.0
Date: 13. 04. 2019
  Major Features:
    - Removed hardened burner mining drill.
    - Added steel and electric hardened furnaces.
    - Removed "Hardened buildings" technology.
    - Added technologies for hardened furnaces.
  Balancing:
    - Changed hardened electric mining drill mining area to 6x6.
    - Hardened stone furnace crafting speed 1.3 ==> 1.25
    - Hardebed stine furnace health 600 ==> 300
    - Nuclear assembling machine max health 500 ==> 450
    - Nuclear assembling machine crafting speed 5 ==> 2
    - Nuclear assembling machine energy usage 500kW ==> 1MW
    - Nuclear assembling machine emissions per minute 2 ==> 1
    - Changed Nuclear assembling machine recipe
  Fixes:
    - Fixed order for Nuclear assembling machine.
    - Fixed order for macerator and compressor.
  Locale:
    - Fixed name for compressor ("compresser" previously).
    - Added Ukrainian locale.
  Graphics:
    - Updated hardened stone furnace textures.
---------------------------------------------------------------------------------------------------
Version: 0.14.11
Date: 06. 03. 2019
  Fixes:
    - Fixed nuclear transport belt animation (lr and hr).
    - Fixed Nuclear construction robot animation.
    - Fixed Nuclear underground transport belt icon.
    - Fixed order for modules.
    - Fixed order for Hardened furnace.
    - Fixed order for nuclear robots.
---------------------------------------------------------------------------------------------------
Version: 0.14.10
Date: 04. 03. 2019
  Fixes:
    - Fixed nuclear transport belt texture name.
---------------------------------------------------------------------------------------------------
Version: 0.14.9
Date: 04. 03. 2019
  Balancing:
    - Speed module 4 speed bonus 0.7 ==> 0.8
    - Speed module 4 consumption bonus 1 ==> 0.8
    - Effectivity module 4 consumption bonus -0.7 ==> -0.6
    - Productivity module productivity bonus 0.12 ==> 0.16
    - Productivity module pollution bonus 0.15 ==> 0.125
    - Speed-Effectivity module speed bonus 0.7 ==> 0.8
    - Speed-Effectivity module consumption bonus 0. 3==> 0.2
    - Productivity-Effectivity module productivity bonus 0.12 == 0.16
    - Productivity-Effectivity module consumption bonus 0.3 ==> 0.4
    - Productivity-Effectivity module pollution bonus 0.15 ==> 0.125
---------------------------------------------------------------------------------------------------
Version: 0.14.8
Date: 04. 03. 2019
  Fixes:
    - Fixed science pack names for module technologies.
---------------------------------------------------------------------------------------------------
Version: 0.14.7
Date: 04. 03. 2019
  Fixes:
    - Fixed missing require() for module-technology tree (https://github.com/JohnTheCoolingFan/RandomFactorioThings/issues/3).
---------------------------------------------------------------------------------------------------
Version: 0.14.6
Date: 26. 02. 2019
  Minor Features:
    - Added thumbnail.
---------------------------------------------------------------------------------------------------
Version: 0.14.5
Date: 26. 02. 2019
  Minor Features:
    - Updated to Factorio 0.17.
---------------------------------------------------------------------------------------------------
Version: 0.14.4
Date: 23. 10. 2018
  Fixes:
    - Fixed non-hr texture path errors.
---------------------------------------------------------------------------------------------------
Version: 0.14.3
Date: 23. 10. 2018
  Fixes:
    - Fixed non-hr texture path errors.
---------------------------------------------------------------------------------------------------
Version: 0.14.2
Date: 22. 10. 2018
  Fixes:
    - Fixed non-hr texture path errors.
---------------------------------------------------------------------------------------------------
Version: 0.14.1
Date: 21. 10. 2018
  Graphics:
    - Graphics improvements for hardened buildings (burner miner and electric miner).
---------------------------------------------------------------------------------------------------
Version: 0.14.0
Date: 16. 10. 2018
  Major Features:
    - Added nuclear drones.
  Minor Features:
    - Rewritten some prototype files.
    - Some localisation improvements.
---------------------------------------------------------------------------------------------------
Version: 0.13.2
Date: 29. 09. 2018
  Fixes:
    - Fixed error on load.
---------------------------------------------------------------------------------------------------
Version: 0.13.1
Date: 29. 09. 2018
  Minor Features:
    - Added integration with Deadlock's mods.
---------------------------------------------------------------------------------------------------
Version: 0.13.0
Date: 24. 09. 2018
  Major Features:
    - Added Speed-Productivity modules.
  Graphics:
    - Updated icons for merged modules.
    - Updated icons for merged modules technologies.
  Balancing:
    - Merged modules research ingredient multiplier 150 ==> 200
---------------------------------------------------------------------------------------------------
Version: 0.12.2
Date: 18. 08. 2018
  Fixes:
    - Fixed error with wrong texture path.
---------------------------------------------------------------------------------------------------
Version: 0.12.1
Date: 15. 08. 2018
  Balancing:
    - Changed merged modules researches 2-3-4 level to include science packs 3
    - Changed merged modules researches 3-4 level time from 30 to 60
    - Changed merged modules researches 3-4 level ingredient multiplier from 50 to 100
    - Replaced production science pack by high-tech science pack in Speed module 4 research.
---------------------------------------------------------------------------------------------------
Version: 0.12.0
Date: 03. 08. 2018
  Major Features:
    - Added Nuclear empowered axe.
    - Added Nuclear empowered logistics research.
    - Added Nuclear empowered transport belt, underground belt and splitter.
  Graphics:
    - Remaked Nuclear inserter, Raw nuclear metal and nuclear metal icons.
    - remaked nuclear-inserter textures.
  Minor Features:
    - "Nuclear automatisation" research renamed to "Nuclear automation".
---------------------------------------------------------------------------------------------------
Version: 0.11.1
Date: 31. 07. 2018
  Fixes:
    - Fixed that game does not load because of my mistake.
---------------------------------------------------------------------------------------------------
Version: 0.11.0
Date: 31. 07. 2018
  Balancing:
    - Hardened buildings technology: ingridient count changed to 100 (90 previously).
    - Nuclear automation technology ingredient count 300 ==> 500
    - Nuclear automation technology time unit 45 ==> 35
    - Added 2x tier 3 assembling machine to nuclear assembling machine recipe.
    - Nuclear assembling machine health 400 ==> 500
    - Nuclear assembling machine crafting speed 10 ==> 5
    - nuclear assembling machine energy consumtion 250kW ==> 500kW
    - Nuclear empowered inserter energy cinsumption per movement and per rotation changed to 15kW
---------------------------------------------------------------------------------------------------
Version: 0.10.4
Date: 30. 07. 2018
  Minor Features:
    - changelog.txt restructurized AGAIN.
---------------------------------------------------------------------------------------------------
Version: 0.10.3
Date: 04. 05. 2018
  Minor Features:
    - Restructurized changelog.txt.
    - Removed dependency on JohnCore.
---------------------------------------------------------------------------------------------------
Version: 0.10.2
Date: 21. 03. 2018
  Minor Features:
    - Nuclear assembling machine and modules order improvements.
---------------------------------------------------------------------------------------------------
Version: 0.10.1
Date: 28. 01. 2018
  Graphics:
    - Added textures for Nuclear empowered inserter.
---------------------------------------------------------------------------------------------------
Version: 0.10.0
Date: 18. 12. 2017
  Major Features:
    - Added Nuclear empowered inserter.
    - Added Nuclear metal and Raw Nuclear metal.
    - Added new recipes in Nuclear Automatisation technology.
  Balancing:
    - Nuclear Automatisation ingredients multiplier extended by 50%.
    - Stack size for Compressed fuel extended to 75.
  Minor Features:
    - Added changelog.txt.
    - Now working on 0.16.
